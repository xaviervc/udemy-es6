/*
/////////////////////////////////////////////////////////
//let and const, are used to declare variables replacements for var

//ES5
var name5 = 'Jane Smith';
var age5 = 23;

name5 = 'Jane Miller';

console.log(name5);

//ES6 Syntax
//const for constant variables, which are immutable, let allows mutability
//var is function scope, let and const are block scope
const name6 = 'Jane Smith';
let age6 = 23;

//ES5 Syntax, variables using var are function scope
function driversLicence5(passedTest) {
    if (passedTest) {
        var firstName = 'John';
        var yearOfBirth = 1990;

    }
    console.log(firstName + ' ' + yearOfBirth + ' can drive car.');
}

driversLicence5(true);

//ES6 Syntax, variables let and const are block scope
function driversLicence6(passedTest) {
    let firstName = 'John';
    const yearOfBirth = 1990;
    if (passedTest) {

        console.log(firstName + ' ' + yearOfBirth + ' can drive car.');
    }
}

driversLicence6(true);


var i = 23;

for (var i = 0; i<5; i++) {
    console.log(i);
}

console.log(i);

*/


/*
/////////////////////////////////////////////////////////////
//Blocks and IIFEs
//simpler way of acheiving data privacy, using blocks

//ES6 syntax
{
    //this is a block
    const a = 1;
    let b = 2;
    //block or not, they are function scoped
    var c = 3;
}

console.log(c);

//ES5 syntax
(function(){
    var c = 25;
})();



*/

/*
////////////////////////////////////////////////
//ES6 Strings

let firstName = 'John';
let lastName = 'Smith';
const yearOfBirth = 1990;

function calcAge(year) {
    return 2019 - year;
}

//ES5 syntax
console.log('This is ' + firstName + ' ' + lastName + '.');


//ES6 syntax, ${variable_here}, no more plusses or minus
console.log(`This is ${firstName} ${lastName} and he is ${calcAge(yearOfBirth)} years old.`);


const n = `${firstName} ${lastName}`;

console.log(n.startsWith('J'));
console.log(n.endsWith('h'));

console.log(n.includes(' '));

console.log(`${firstName} `.repeat(5));
*/

/*
/////////////////////////////////////////
//Arrow functions

const years = [1990, 1965, 1982, 1937];

//to loop over array you could use map
var ages5 = years.map(function(el) {
    return 2019 - el;
});

console.log(ages5);


//ES6 
let ages6 = years.map(el => 2019 - el);

console.log(ages6);

ages6 = years.map((el, index) => `Age element ${index+1}: ${2019 - el}`);

console.log(ages6);

ages6 = years.map((el, index) => {
   const now = new Date().getFullYear();
   const age = now - el;
   return `Age element ${index + 1}: ${age}`;
});

console.log(ages6);

*/
//////////////////////////////////////
//Arrow functions part 2, lexical this variable

//ES5 syntax
/*
var box5 = {
    color: 'green',
    position: 1,
    clickMe: function() {

        var self = this;

        document.querySelector('.green').addEventListener('click', function() {
            var str = 'This is box number ' + self.position + ' and it is '+ self.color;
            alert(str);
        });
    }
}

// box5.clickMe();

//ES6 syntax

const box6 = {
    color: 'green',
    position: 1,
    clickMe: () => {
        document.querySelector('.green').addEventListener('click', (this) => {
            var str = 'This is box number ' + this.position + ' and it is '+ this.color;
            alert(str);
        });
    }
}

box6.clickMe();

*/


/*
///////////////////////////////////////////////
//Arrow functions part 2, lexical this, this keyword

function Person(name) {
    this.name = name;
}

Person.prototype.myFriends5 = function (friends) {
    
    var arr = friends.map(function(el) {
        return this.name + ' is friends with ' + el;
    }.bind(this));

    console.log(arr);

}

var friends = ['bob', 'jane', 'marc'];

//new Person('John').myFriends5(friends);


//ES6 Version

Person.prototype.myFriends6 = function (friends) {
    
    var arr = friends.map(el => `${this.name} is friends with ${el}`);

    console.log(arr);

}

var friends = ['bob', 'jane', 'marc'];

new Person('John').myFriends6(friends);
*/

/*
////////////////////////////////
//Destructuring

//ES5
var john = ['John', 26];

// var name = john[0];
// var age = john[1];

//ES6 syntax, destructuring the data structure
const [name, age] = ['John', 26];
console.log(name);
console.log(age);

const obj = {
    firstName: 'John',
    lastName: 'Smith'
};

//make sure it matches the object params
const {firstName, lastName} = obj;

console.log(firstName, lastName);

//assigning to variables
const {firstName: a, lastName: b} = obj;

console.log(a, b);


function calcAgeRetirement(year) {
    const age = new Date().getFullYear() - year;
    return [age, 65 - age];
}

const [age2, retirement] = calcAgeRetirement(1990);

console.log(age2, retirement);

*/
/*
//////////////////////////////////
//Arrays

//returns a node list
const boxes = document.querySelectorAll('.box');

//ES5 syntax

var boxesArr5 = Array.prototype.slice.call(boxes);

boxesArr5.forEach(function(cur) {
    cur.style.backgroundColor = 'dodgerblue';
})


//ES6
const boxesArr6 = Array.from(boxes);

boxesArr6.forEach(el => {
    el.style.backgroundColor = 'dodgerblue';
});

//ES5
/*
for(var i=0; i<boxesArr5.length; i++) {

    if(boxesArr5[i].className === 'box blue') {
        // continue;
        break;
    }

    boxesArr5[i].textContent = 'I changed to blue!';

}
*/

/*

//ES6
for (const cur of boxesArr6) {
    if (cur.className.includes('blue')){
        continue;
    }
    cur.textContent = 'I changed to blue!';

}

////////////////////////
//ES5

var ages = [12, 17, 8, 21, 14, 11];

var full = ages.map(function(cur){
    return cur >= 18;
});

console.log(full);

console.log(full.indexOf(true));

//ES6

console.log(ages.findIndex(cur => cur >= 18));
console.log(ages.find(cur => cur >= 18));

*/

//////////////////////////////////////
//Spread operator
/*
function addFourAges(a, b, c, d) {
    return a + b + c + d;
}

var sum1 = addFourAges(18, 30, 12, 21);

console.log(sum1);

//ES5
var ages = [18, 30, 12, 21];

var sum2 = addFourAges.apply(null, ages);

console.log(sum2);

//ES6, tripple dots means expand into components, spread operator
const sum3 = addFourAges(...ages);
console.log(sum3);

const familySmith = ['John', 'Jane', 'Mark'];

const familyMiller = ['Mary', 'Bob', 'Ann'];

const bigFamily = [...familySmith, ...familyMiller];

console.log(bigFamily);

//spread can be used on a node list
const h = document.querySelector('h1');

const boxes = document.querySelectorAll('.box');

const all = [h, ...boxes];

Array.from(all).forEach(cur => cur.style.color = 'purple');
*/



///////////////////////////////////
//Rest Parameters, pass arbitrary number of parameters
/*
//ES5, the arguments variable is an object.
function isFullAge() {
    var argsArr = Array.prototype.slice.call(arguments);
    
    argsArr.forEach(function(cur){
        console.log(2019-cur);
    });
}

isFullAge(1992, 2001, 2002);

//ES6, triple dots is a rest parameter meaning that 

function isFullAge6(...years){
    years.forEach(cur => console.log((2019 - cur) >= 18));
}

isFullAge6(1990, 2002, 1992);

*/

/*

function isFullAge(limit) {
    var argsArr = Array.prototype.slice.call(arguments, 1);

    argsArr.forEach(function(cur){
        console.log((2019-cur)>=limit);
    });
}

isFullAge(1990, 2002, 1992);



function isFullAge6(limit, ...years){
    years.forEach(cur => console.log((2019 - cur) >= limit));
}

isFullAge6(1990, 2002, 1992);

*/

/*

//////////////////////////////////////////////
//Default parameters

//ES5
function SmithPerson(firstName, yearOfBirth, lastName, nationality) {

    //default parameters
    lastName === undefined ? lastName = 'Smith' : lastName = lastName;
    nationality === undefined ? nationality = 'American' : nationality = nationality;

    this.firstName = firstName;
    this.lastName = lastName;
    this.yearOfBirth = yearOfBirth;
    this.nationality = nationality;
}

var john = new SmithPerson('John', 1990);

//ES6
function SmithPerson6(firstName, yearOfBirth, lastName = 'Smith', nationality = 'american'){
    this.firstName = firstName;
    this.lastName = lastName;
    this.yearOfBirth = yearOfBirth;
    this.nationality = nationality;
}

*/

/////////////////////////////////////////////////////
// Maps

// const question = new Map();

// question.set('question', 'What is the official name of the latest major JS version?');
// question.set(1, 'ES5');
// question.set(2, 'ES6');
// question.set(3, 'ES2015');
// question.set(4, 'ES7');
// question.set('correct', 3);
// question.set(true, 'Correct answer');
// question.set(false, 'Wrong, please try again!');

// //get function
// // console.log(question.get('question'));
// // console.log(question.size);

// // if(question.has(4)){
// //     question.delete(4);
// // }

// // question.clear();

// //maps are iterable

// // question.forEach((value, key) => {
// //     console.log(`${key}: ${value}`);
// // })

// //destructuring syntax
// for (let [key, value] of question.entries()){
//     // console.log(`${key}: ${value}`);
//     if(typeof(key) === 'number'){
//         console.log(`Answer ${key}: ${value}`);
//     }
// }

// const ans = parseInt(prompt('Write the correct answer'));

// console.log(question.get(ans === question.get('correct')));


////////////////////////////////////////////////////////////////////
/////// Classes

//ES5 syntax
// var Person5 = function(name, yearOfBirth, job) {
//     this.name = name;
//     this.yearOfBirth = yearOfBirth;
//     this.job = job;
// }

// Person5.prototype.calculateAge = function() {
//     var age = new Date().getFullYear - this.yearOfBirth;
//     console.log(age);
// }

// var john5 = new Person5('John', 1990, 'teacher');

// class Person6 {
    
//     constructor (name, yearOfBirth, job) {
//         this.name = name;
//         this.yearOfBirth = yearOfBirth;
//         this.job = job;
//     }

//     calculateAge() {
//         var age = new Date().getFullYear - this.yearOfBirth;
//     }

//     static greeting() {
//         console.log('Test');
//     }

// }

// const jonh6 = new Person6('John', 1990, 'teacher');

// Person6.greeting();


////////////////////////////////////////////////////////////////
///////// Subclasses

var Person5 = function(name, yearOfBirth, job) {
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;
}

Person5.prototype.calculateAge = function() {
    var age = new Date().getFullYear() - this.yearOfBirth;
    console.log(age);
}

var john5 = new Person5('John', 1990, 'teacher');

var Athlete5 = function(name, yearOfBirth, job, olympicGames, medals) {
    Person5.call(this, name, yearOfBirth, job);
    this.olympicGames = olympicGames;
    this.medals = medals;
}



Athlete5.prototype = Object.create(Person5.prototype);

Athlete5.prototype.wonMedal = function() {
    this.medals++;
    console.log(this.medals);
}

var johnAthlete5 = new Athlete5('John', 1990, 'swimmer', 3, 10);

johnAthlete5.calculateAge();

//ES6

class Person6 {
    
    constructor (name, yearOfBirth, job) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.job = job;
    }

    calculateAge() {
        var age = new Date().getFullYear() - this.yearOfBirth;
    }

}

class Athlete6 extends Person6 {
    constructor(name, yearOfBirth, job, olympicGames, medals) {
        super(name, yearOfBirth, job);
        this.olympicGames = olympicGames;
        this.medals = medals;
    }

    wondMedal() {
        this.medals++;
        console.log(this.medals);
    }
}

const johnAthlete6 = new Athlete6('John', 1990, 'swimmer', 3, 10);

johnAthlete6.wondMedal();
johnAthlete6.calculateAge();





